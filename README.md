# Welcome to my personal vim configuration

this is a work in progress (I learn something new in Vim every day!)
this is mainly here for me to pull into new environments and unpack
if you would like to install this to try it out, just a few things...

* this set-up is pretty minimal (the way I like it!)
* this set-up has FIGlet for vim installed, so make sure you have FIGlet installed if you'd like to use it.
* to use, simply clone repo and unpack '.vim' and '.vimrc' into your home directory
